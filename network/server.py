#网络编程
#server
import socket

s = socket.socket()
host = socket.gethostname()
port = 1234
#服务器调用bind方法
s.bind((host,port))
#在调用listen方法
s.listen(5)
while True:
    #阻塞，知道客户端的连接，返回client，address的元组
    c,addr = s.accept()
    print('Got connection from ',addr)
    c.send(b'Tannk you fro connection')
    c.close()
