#SockServer

from socketserver import TCPServer,StreamRequestHandler

class Handler(StreamRequestHandler):
    def handle(self):
        addr = self.request.getpeername()
        print(b'got connection from ',addr)
        self.wfile.write(b'Thank you for connection')
server = TCPServer(('',1234),Handler)
server.serve_forever()
