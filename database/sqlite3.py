#database sqlite3
#使 用sqlite的connect可以连接一个数据库文件，当数据库文件不存在的时候，它会自动创建。如果已经存在这个文件，则打开这个文件。cx为数据库连接对象。 
import sqlite3 #导入模块  
cx = sqlite3.connect("d:\\test.db")
"""象前面的cx就是一个数据库的连接对象，它可以有以下操作： 

         commit()--事务提交 
         rollback()--事务回滚 
         close()--关闭一个数据库连接 
         cursor()--创建一个游标 """
"""3.2 游标对象 所有sql语句的执行都要在游标对象下进行。 

cu = cx.cursor()#这样定义了一个游标。
游标对象有以下的操作： 

        execute()--执行sql语句 
        executemany--执行多条sql语句 
        close()--关闭游标 
        fetchone()--从结果中取一条记录 
        fetchmany()--从结果中取多条记录 
        fetchall()--从结果中取出多条记录 
        scroll()--游标滚动 """
cu=cx.cursor()   
u.execute("""create table catalog ( id integer primary key, pid integer, name varchar(10) UNIQUE )""")

cu.execute("insert into catalog values(0, 0, 'name1')")   
cu.execute("insert into catalog values(1, 0, 'hello')")   
cx.commit()

cu.execute("select * from catalog")   
print(cu.fetchall())

cu.execute("select * from catalog where id = 1")   
print(cu.fetchone())

cu.execute("update catalog set name='name2' where id = 0")   
cx.commit()   
cu.execute("select * from catalog")   
print(cu.fetchone())

cu.execute("delete from catalog where id = 1")   
cx.commit()   
cu.execute("select * from catalog")   
cu.fetchall()   
#cu.close()  
#cx.close()   
