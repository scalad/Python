#对文件进行迭代,
#在这部分所有的例子中都使用了一个名为process的函数，用来表示每个字符或每行的处理过程
def process(string):
    print('Processing:',string)
    
#按字节处理
filename = r'f:\test.txt'
f = open(filename)
char = f.read(1)
while char:
    process(char)
    char = f.read(1)
f.close()
print('------------------------使用while True--------------------------')
f = open(filename)
while True:
    char = f.read(1)
    if not char:break
    process(char)
f.close()

#按照行进行操作
f = open(filename)
while True:
    line = f.readline()
    if not line:break
    process(line)
f.close()

#读取所有的内容,如果文件不是很大，可以一次性的把该文件读取进内存
#使用read迭代每个字符
f = open(filename)
for char in f.read():
    process(char)
f.close()
#使用readlines迭代行
f = open(filename)
for line in f.readlines():
    process(line)
f.close()

#使用fileinput实现懒惰迭代
#在需要对一个非常大的文件进行迭代操作时，readlines会占用太多的内存。这个时候可以使用while和readline方法来替代
import fileinput
for line in fileinput.input(filename):
    process(line)

#文件迭代器
print('使用文件迭代器')
f = open(filename)
for line in f:
    process(line)

#对文件进行迭代而不适用变量存储文件对象
for line in open(filename):
    process(line)
print('对sys.stdin进行迭代')
#sys.stdin是可以迭代的
import sys
for line in sys.stdin:
    pricess(line)
    
f.close()
