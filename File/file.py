#open函数用来打开文件open(name[,mode[,buffering]])，模式和缓冲参数是可选的
f = open(r'f:\test.txt')
#文件打开模式r w a b +模式  rb读取一个二进制文件
#缓冲如果参数是0，表示所有操作都是直接针对硬盘直接读写的，如果是1或者True，使用内存来替代硬盘，是有使用flush或者close时才会更新到硬盘上的数据

#文件的读写
f = open(r'f:\somefile.txt','w')
f.write('hello, ')
f.write('World!')
f.close()

f = open(r'f:\test.txt','r')
result = f.read(4)#读取多少字符
print(result)
print(f.read())
f.close()

#读写行,使用file.readline读取单独的一行
print('使用readline函数读取文件')
f = open(r'f:\test.txt','r')
for i in range(3):
    print(i,':',f.readline())
f.close()

#使用readlines()
print('使用了readlines()函数读取文件')
import pprint
pprint.pprint(open(r'f:\test.txt').readlines())

#关闭文件
#try:
    #witer data to your file
#finally:
    #file.close()
#有专门设计的语句
#with open('somefile.txt') as somefile:
#    do_something(somefile)
#with语句实际上是通用的结构，允许使用所谓的上下文管理器，上下文管理器是一种支持__enter__和__exit__这两个方法的对象

#写文件
f = open(r'f:\test1.txt','w')
f.write('this\nis no\nhaiku')
f.close()

#修改文件writelines
f = open(r'f:\test1.txt')
lines = f.readlines()
f.close()
lines[1] = 'is not a\n'
f = open(r'f:\test1.txt','w')
f.writelines(lines)
f.close()


