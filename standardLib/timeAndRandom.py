#time get current time   [2008,1,21,12,2,56,0,21,0]
import time
#将当前时间格式化,asctime()将时间元组转化为字符串
print(time.asctime())

#localtime([secs])将秒数转化为日期元组
print(time.localtime(1010101010))
#time.struct_time(tm_year=2002, tm_mon=1, tm_mday=4, tm_hour=7, tm_min=36, tm_sec=50, tm_wday=4, tm_yday=4, tm_isdst=0)

#mktime(tuple)将时间元组传唤为本地时间
datatime = (2008,1,21,12,2,56,0,21,0)
print(time.mktime(datatime))

#sleep(secs)休眠secs秒,什么事情也不做
#time.sleep(5)

#strptime(string[,format])将字符串解析为时间元组

#time()当前的时间
print(time.time())


#random
import random
print(random.random)
from random import *
from time import *
date1 = (2008,1,1,0,0,0,-1,-1,-1)
time1 = mktime(date1)
date2 = (2009,1,1,0,0,0,-1,-1,-1)
time2 = mktime(date2)
random_time = uniform(time1,time2)#在两个时间段内 产生随机的时间
print(random_time)
print(asctime(localtime(random_time)))

#chice(seq)返回seq中随意元素
print(choice([1,2,3,4,5]))

#sample(seq,n)从序列seq中选择n个随机并且独立的元素
print(sample([1,2,3,43,5,6,56,675,45],3))

#getrandbits(n)以长整形方式返回n个随机数
print(getrandbits(6))

#randrange([start],stop,[step])返回range(start,stop,step)中的随机数
#获取小于20的随机数
print(randrange(1,20,2))

from random import randrange
num = input('how many dice?')
sides = input('how many sides per die?')
sum = 0
for i in range((int)(num)):
    sum+=randrange(int(sides))+1
print('the result is:',sum)





