#re模块包含一些有关正则表达式的函数
#compile(pattern[,flag])根据包含正则表达式的字符串创建模式对象
#re.compile,如果在调用search或者match函数的时候使用字符串表示的正则表达式，它们会在内部
#将字符串转为正则表达式对象。使用compile完成一次转换后，在以后每次使用模式的时候就不用再进行转换
#re.search(pat,string) == pat.search(string)
import re
#re.match会在给定的字符串开头匹配正则表达式
print(re.match('py','python'))

#函数re.split()会根据模式的匹配项来分割字符串
some_text = 'alpha,baba,,,,gamma delta'
result = re.split('[, ]+',some_text)
print(result)

#maxsplit参数表示字符串最多可以分割的次数
result = re.split('[, ]+',some_text,maxsplit=2)
print(result)
result = re.split('[, ]+',some_text,maxsplit=1)
print(result)

#函数findall以列表形式返回给定模式的所有匹配项
pat = '[a-zA-Z]+'
text = 'Hm.. Err-- are you sure? he said.sounding insecure'
result = re.findall(pat,text)
print(result)
#或者查找标点符号 \-表示不会将其解释为字符范围的一部分
pat = r'[.?\-",]+'
result = re.findall(pat,text)
print(result)

#re.sub函数 使用给定的替换内容将匹配模式的子字符串替换掉
pat = '{name}'
text = 'Dear {name},your name is {name}'
print(re.sub(pat,'Silence',text))

#re.escape函数可以对字符串中所有可能被解释为正则运算符的字符进行转义的应用函数，
#如果字符串很长切包含很多特殊字符，而你又不想输入一大堆反斜杠
print(re.escape('www.python.org'))
print(re.escape('But where is the ambiguity?'))


#匹配对象和组   组就是放置在圆括号内的子模式，组的序号取决于它左侧的括号数
m = re.match(r'www\.(.*)\..{3}','www.python.org')
print(m.group(1))#获取给定子模式的匹配项
print(m.start(1))#返回给定 组的匹配项的开始位置
print(m.end(1))#返回给定组的匹配项的结束的位置
print(m.span(1))#开始和结束的位置


#作为替换的组号和函数     #^\*捕捉*号以外的任何串
emhasis_pattern = r'\*([^\*]+)\*'
result = re.sub(emhasis_pattern,r'<em>\1</em>','hello *world!*')
print(result)



