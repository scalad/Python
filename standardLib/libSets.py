#sets集合Set类位于sets模块中
print(set(range(10)))

#union
a = set([1,2,3])
b = set([2,3,4])
print(a.union(b))

c = a & b
print(c)
print(c.issubset(a))

print(c<=a)

print(a.intersection(b))

print(a.difference(b))

print(b - a)


print(a.symmetric_difference(b))

print(a ^ b)

print(a.copy() is a)

#不
