#标准库
#sys
import sys
args = sys.argv[1:]
args.reverse()
print(''.join(args))
print(' '.join(reversed(sys.argv[1:])))

#os
import os
print(os.sep)#path
print(os.pathsep)#字符串分割符
#system可以执行外部程序
os.system(r'D:\kugou\KuGou.exe')
os.startfile(r'C:\Windows\System32\cmd.exe')
