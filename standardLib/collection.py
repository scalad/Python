#集合的使用
print(set(range(10)))
print(set([0,1,2,3,4,5,6,7,8,9]))
print(set(['fee','fie','foe']))

a = set([1,2,3])
b = set([2,3,4])
print(a.union(b))

c = a & b
print(c)
print(c.issubset(a))

print(c <= a)
print(c >= a)

print(a.intersection(b))

print(a.difference(b))
print(a - b)

print(a.symmetric_difference(b))
print(a ^ b)

print(a.copy() is a)

#不可变的集合
a = set([1,2])
b = set([3,4])
a.add(frozenset(b))
print(a)
