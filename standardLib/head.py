#事实上python没有独立的堆类型，只有一个包含一些堆操作的模块，这个模块叫做headq
#heapush(head,x)     将x入堆
#heapop(head)        将堆中最小的元素弹出
#heaify(head)        将head属性强制应用到任意一个列表
#heapreplace(head,x) 将堆中最小的元素弹出，同时将x如堆
#nlargest(n,iter)    返回iter中第n大的元素
#nsmallest(n,iter)   返回iter中第n小的元素

from heapq import *
from random import shuffle
data = list(range(10))
shuffle(data)
heap= []
for n in data:
    heappush(heap,n)
print(heap)

print(heappop(heap))
print(heap)
print(heappop(heap))
print(heap)

#heapify
heap = [6,7,3,1,2,3,8,9]
heapify(heap)
print(heap)

#heapreplace弹出最小的元素，并且将新元素加入
heapreplace(heap,0.5)
print(heap)

#双端队列   collections模块
from collections import deque
q = deque(range(5))
q.append(5)
q.appendleft(6)
print(q)
print(q.pop())
print(q)
q.popleft()
q.rotate(3)#使用双端队列进行循环左移或者右移
q.rotate(-1)
print(q)

