soundrel = {'name':'Silence','girlfriend':'Marion'}
key,value = soundrel.popitem()
print(key)

#if elif
num = input('enter a number')
if int(num) > 0 and int(num) < 10:
    print('The number is positive')
elif int(num) < 0:
    print('The number is negative')
else:
    print("The number is zero")


#is运算符
x = [1,2,3]
y = [1,2,3]
print(x == y)
print(x is y)
print(x is not y)
#True
#False
#True

#in操作符
name = '123'
if '1' in name:
    print('in')
else:
    print('not')

#and
number = input('enter a numbner 1 and 10:')
if int(number) <= 10 and int(number) >= 1:
    print('Great!')
else:
    print('wrong')

#断言,程序在错误条件下直接让他崩溃
age = 10
assert 0 < age < 100
#age = -1
#assert 0 < age < 100
#Traceback (most recent call last):File "<pyshell#7>", line 1, in <module>assert 0 < age < 100AssertionError




