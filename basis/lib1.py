#fileinput                               #  1
import fileinput                         #  2
#inplace=True  using the native handler  #  3
for line in fileinput.input(inplace=1):  #  4
    line = line.rstrip()                 #  5
    num = fileinput.lineno()#get the current line number #  6
    print('%-40s # %2i' % (line,num))    #  7
