#append method
lst = [1,2,3]
lst.append(4)
print(lst)

#count method
count = ['to','be','or','not','to','be'].count('to')
print(count)

#extend methos
a=[1,2,3]
b=[4,5,6]
a.extend(b)
print(a)

a=[1,2,3]
b=[4,5,6]
a[len(a):] = b
print(a)

#index method
#当列表中没有该元素时报异常
knights = ['We','are','the','knights','who','say','ni']
print(knights.index('who'))

#insert method
numbers=[1,2,3,5,6,7]
numbers.insert(3,'four')
print(numbers)

#pop method
x = [1,2,3]
x.pop()
print(x)

x = [1,2,3]
x.append(x.pop())
print(x)

#remove method
x = ['to','be','or','not','to','be'];
x.remove('to')
print(x)
x.remove('to')
print(x)

#reverse method
x=[1,2,3]
x.reverse()
print(x)

#sort method
x = [4,6,3,1,78,9,6,5]
y=x[:]
y.sort();
print(x)
print(y)

#sorted method
x = [4,6,3,1,78,9,6,5]
y = sorted(x)
print(x)
print(y)

print("sort(reverse=True)")
x = [4,6,3,1,78,9,6,5]
x.sort(reverse=True)
print(x)

#元组，实现一个元组必须加逗号
x = 42,
print(x)
print(3*(40+2,))

#tuple method
print(tuple([1,2,3]))
print(tuple(['a','b','c']))






