#迭代器Iterator,对对象进行迭代，只要该对象实现了__iter__方法
class Fibs:
      def __init__(self):
            self.a = 0
            self.b = 1
      def __next__(self):
            self.a,self.b = self.b,self.a + self.b
            return self.a
      def __iter__(self):
            return self
#内建的函数__iter__还可以从迭代的对象中获得迭代器
it = iter([1,2,3])
print(it.__next__())
print(it.__next__())
#从迭代器中获得序列
class TestIterator:
      value = 0
      def __next__(self):
            self.value += 1
            if self.value > 10:
                  raise StopIteration
            return self.value
      def __iter__(self):
            return self
      
