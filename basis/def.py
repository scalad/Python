#abstruct function
def fibs(num):
    result = [0,1]
    for i in range(num - 2):
        result.append(result[-2] + result[-1])
    return result

#文档化函数
def square(x):
    'Calculates the sequre of the number x.'
    return x*x
#文档串可以按照以下的方式进行访问
print(square.__doc__)
#help函数可以帮助我们得到文本串的信息
help(square)

def test():
    print('this is printed')
    return
    print('this is not')
x = test()
print(x)


