def checkIndex(key):
      if not isinstance(key,(int,long)):
            raise TypeError
      if key < 0:
            raise IndexError
class ArithmeticSequence:
      def __init__(self,start=0,step=1):
            self.start = start
            self.step = step
            self.changed = {}
      def __getitem__(self,key):
            """Get an item from the arithmetic sequence."""
            checkIndex(key)
            try:
                  return self.changed[key]
            except KeyError:
                  return self.start + key * self.step
      def __setitem__(self,key,value):
            checkIndex(key)
            self.changed[key] = value
#子类化列表、字典和字符串
#当子类化一个内建类型--list的时候，也就间接的将object子类化了，因此
#该类就自动成为新式类，CounterList严重依赖它的超类list的行为
class CounterList(list):
      def __init__(self,*args):
            super(CounterList,self).__init__(*args)
            self.counter = 0
      def __getitem__(self,index):
            self.counter += 1
            return super(CounterList,self).__getitem(index)
c1 = CounterList(range(10))
print(c1)
c1.reverse()
print(c1)
del c1[3:6]
print(c1)
print(c1.counter)

#属性
class Rectangle:
      def __init__(self):
            self.width = 0
            self.height = 0
      def setSize(self,size):
            self.width,self.height = size
      def getSize(self):
            return self.width,self.height
r = Rectangle()
r.width = 10
r.height = 5
print(r.getSize())
r.setSize((120,200))
print(r.getSize())

#property函数
__metaclass__= type
class Rectangle:
      def __init__(self):
            self.width = 0
            self.height = 0
      def setSize(self,size):
            self.width,self.height = size
      def getSize(self):
            return self.width,self.height
      size = property(getSize,setSize)
r = Rectangle()
r.width = 10
r.height = 5
print(r.size)
r.size = 150,100
print(r.height)
