#列表推倒式
x = [x*x for x in range(10)]
print(x)

x = [x*x for x in range(10) if x % 3 == 0]
print(x)

x = [(x,y) for x in range(3) for y in range(3)]
print(x)

result = []
for x in range(3):
    for y in range(3):
        result.append((x,y))
print(result)

girls = ['alice','bernice','clarice']
boys = ['chris','arnold','bob']
x = [b + "+" + g for b in boys for g in girls if b[0] == g[0]]
print(x)
#男孩女孩名字对的例子其实效率并不高，因为它会检查每个可能的配对
