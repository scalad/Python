#生成器
def flatten(nested):
    for sublist in nested:
        for element in sublist:
            yield element
#包含yeild语句的函数成为生成器，每次产生一个值，函数就会被冻结
nested = [[1,2],[3,4],[5]]
for num in flatten(nested):
    print(num)
#使用生成器处理多层循环
def flatten1(nested):
    try:
        for sublist in nested:
            for element in flatten(sublist):
                yield element
    except TypeError:
        yield nested
nested = [[1,2,[6,7,[8]]],[3,4],[5]]
print(list(flatten1(nested)))

    
def g(n):
    for i in range(n):
        yield i ** 2
for i in g(5):
    print(i,":")
#一个yield的例子，用生成器生成一个Fibonacci数列
def fab(max):
    a,b=0,1
    while a < max:
        yield a
        a,b=b,a+b
