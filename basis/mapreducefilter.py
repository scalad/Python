#这个函数的主要功能是对一个序列对象中的每一个元素应用被传入的函数，
#并且返回一个包含了所有函数调用结果的一个列表。
#map function
counters = [1,2,3]
def inc(x):
    return x + 10
result = list(map(inc,counters))
print(result)

print(list(map(lambda x : x * 2,[1,2,3,4,[5,6,7]])))

def abc(a,b,c):
    return a*1000 + b*100 + c
list1 = [11,22,33]
list2 = [44,55,66]
list3 = [77,88,99]
result = list(map(abc,list1,list2,list3))
print(result)

#zip function
#函式说明：zip(seq1[,seq2 [...]])->[(seq1(0),seq2(0)...,(...)]。
#同时循环两个一样长的函数，返回一个包含每个参数元组对应元素的元组。若不一致，采取截取方式，
#使得返回的结果元组的长度为各参数元组长度最小值。
for x,y in zip([1,2,3],[4,5,6]):
    print(x,y)
for x,y in zip([1,2,3],[4,5]):
    print(x,y)
for x,y in zip(range(5),range(6)):
    print(x,y)

#filter function
#filter(bool_func,seq)：此函数的功能相当于过滤器。
#调用一个布尔函数bool_func来迭代遍历每个seq中的元素，返回一个使bool_seq返回值为true的元素的序列。
print(list(filter(lambda x : x % 2 == 0,range(10))))

#reduce function
#reduce(func,seq[,init])：func为二元函数，将func作用于seq序列的元素，每次携带一对
#（先前的结果以及下一个序列的元素），连续的将现有的结果和下一个值作用在获得的随后的结果上，
#最后减少我们的序列为一个单一的返回值：如果初始值init给定，第一个比较会是init和第一个序列元素而不是序列的头两个元素。
from functools import reduce
x = reduce(lambda x,y:x+y,[1,2,3,4,5,6])
print(x)
