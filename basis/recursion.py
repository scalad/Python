#Recursion function
def factorial(n):
    result = n
    for i in range(1,n):
        result *= i
    return result
def factorial1(n):
    if n == 1:
        return 1
    else:
        return factorial1(n - 1) * n
def power(x,n):
    if n == 0:
        return 1
    else:
        return x * power(x,n - 1)

# aeert 质数
def isPrime(n):
    """This function return a number is a prime or not"""
    assert n >= 2
    from math import sqrt
    for i in range(2, int(sqrt(n))+1):
        if n % i == 0:
            return False
    return True

#二分
def search(sequence,number,lower=0,upper=None):
    if upper is None:upper = len(sequence) - 1
    if lower == upper:
        assert number == sequence[upper]
        return upper
    else:
         middle = (lower + upper) // 2
         if number > sequence[middle]:
             return search(sequence,number,middle+1,upper)
         else:
             return search(sequence,number,lower,middle)
            
            
