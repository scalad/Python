#using modul,将模块放在正确的位置，可以在环境变量中添加PYTHONPATH来指定文件的路径
import sys,pprint
pprint.pprint(sys.path)

#包,它必须包含一个命名为__init__.py的文件
#drawing/__init__.py
#drawing/colors.py
#drawing/shaps.py

#模块中有什么
#dir会将对象的所有特性都一一列出
import copy
print(dir(copy))
print('-----------------------------------------------')
result = [n for n in dir(copy) if not n.startswith('_')]
print(result)

#使用help获取
print(help(copy.copy))
print(copy.copy.__doc__)

#使用源代码
print(copy.__file__)
