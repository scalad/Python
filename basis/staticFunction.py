#静态方法和类成员方法分别在创建时分别被装入Staticmethod类型
#和Classmethod类型的对象中，静态方法没有self参数，而且能被本身直接调用
__metaclass = type
#使用手动包装盒替换的方式
class Myclass:
      def smeth():
            print("This is a static method")
      smeth = staticmethod(smeth)
      def cmeth(cls):
            print("This is a class method of ",cls)
            cmeth = classmethod(cmeth)
Myclass.smeth()
#使用装饰器装饰
class Myclass1:
      @staticmethod
      def smeth():
            print("This is a static method")
      @classmethod
      def cmeth(cls):
            print("This is a class method of ",cls)
Myclass1.smeth()

