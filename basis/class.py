#abstruct class
__metaclass__ = type
class Person:
    def setName(self,name):
        self.name = name
    def getName(self):
        return self.name
    def greet(self):
        print('hello,world ! I am %s' % self.name)
foo = Person()
foo.setName('silence')
print(foo.getName())
print(foo.name)
foo.greet()

#让方法或者特性变为私有，则只要在它的名字前面加上双下划线
class Sercetive:
    def __inaccessible(self):
        print('Bet you can not see me...')
    def accessible(self):
        print('The secrent message is..:')
        self.__inaccessible()
s = Sercetive()
#s.__inaccessible()
s.accessible()

#指定超类
class Filter:
    def init(self):
        self.blocked = []
    def filter(self,sequence):
        return [x for x in sequence if x not in self.blocked]
#将其父类的类名写在class语句的圆括号中
class SPAMFilter(Filter):
    def init(self):
        self.blocked = ['SPAM']
f = Filter()
f.init()
#事实上Filter不能过滤什么
x = f.filter([1,2,3])
print(x)
s = SPAMFilter()
s.init()
x = s.filter(['SPAM','SPAM','SPAM','SPAM','eggs','bacon','SPAM'])
print(x)

#检查某个类是否是另一个类的子类
print(issubclass(SPAMFilter,Filter))
print(issubclass(Filter,SPAMFilter))
#已知类的基类
print(SPAMFilter.__bases__)
#检查一个对象是否是一个类的实例
s = SPAMFilter()
print(isinstance(s,SPAMFilter))
#一个对象属于哪个类
print(s.__class__)

#多个超类
class Calculator:
    def calculate(self,expression):
        self.value = eval(expression)
class Talker:
    def talk(self):
        print('Hi,my value is',self.value)
class TalkingCalculator(Calculator,Talker):
    pass
tc = TalkingCalculator()
tc.calculate('1+2+3')
tc.talk()









