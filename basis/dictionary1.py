#dictionary method
#items,iteritems methods
d = {'title':'python web site','url':'http://www.python.org','spam':0}
print(d.items())
#iteritems会返回一个迭代器对象而不是一个列表
#it = d.iteritems()
#print(it)
#print(list(it))

#keys iterkeys
print(d.keys())

#pop
d = {'x':1,'y':2}
print(d)
print(d.pop('x'))
print(d)
#{'y': 2, 'x': 1}
#1
#{'y': 2}

#popitm
d = {'title':'python web site','url':'http://www.python.org','spam':0}
print(d.popitem())
print(d)
#('title', 'python web site')
#{'url': 'http://www.python.org', 'spam': 0}

#setdefault获得与给定健相关联的值
d = {}
d.setdefault('name','N/A')
print(d)
d['name'] = 'Gumby'
d.setdefault('name','N/A')
print(d)
#{'name': 'N/A'}
#{'name': 'Gumby'}

#update
d = {'title':'python web site','url':'http://www.python.org','changed':'Mar 14 22:09:15 MET 2008'}
x = {'title':'Python Language WebSite'}
d.update(x)
print(d)
#{'url': 'http://www.python.org', 'title': 'Python Language WebSite', 'changed': 'Mar 14 22:09:15 MET 2008'}

#values itervalues
print(d.values())
