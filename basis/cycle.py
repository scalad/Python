#while
x = 1
while x <= 10:
    print(x)
    x += 1
    
name = ''
while not name or name.isspace():
    name = input('please enter your name:')
print('Hello %s !' % name)

#for
words = ['this','is','an','ex','parront']
for word in words:
    print(word)

numbers = range(1,10,2)
for number in numbers:
    print(number)

for number in range(1,10):
    print(number)

d = {'x':1,'y':2,'z':3}
for key in d:
    print(key ,'corresponds to ',d[key])

#iterator
names = ['anne','bech','george','damon']
ages = [12,34,65,23]
for i in range(len(names)):
    print(names[i],',is ',ages[i],',years old')
#zip函数可以并行迭代，可以把两个序列压缩在一起，然后返回一个元组的列表
print('using the zip() function')
for name,age in zip(names,ages):
    print(names[i],',is ',ages[i],',years old')

strings = 'walking in the sun you can'
index = 0
for string in strings:
    if 'in' in string:
        string[index] = 'censored'
    index += 1
print(strings)

print(list(reversed('Hello,world!')))
print(''.join(reversed('Hello,world!')))

#break
from math import sqrt
for n in range(99,0,-1):
    root = sqrt(n)
    if root == int(root):
        print(n)
        break

#continue

while True:
    word = input('please enter a word')
    if not word:break
print('the word was ' + word)




