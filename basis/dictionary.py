#this is the dictionary
#dict function
items=[('name','Gumby'),('age',42)]
d = dict(items)
print(d)
d = dict(name='Silence',age=23)
print(d)
x={}
x[42]="Foobar"
print(x)

people={
    'Alice':{
        'phone':'2314',
        'addr':'Foo Driver 23'
        },
    'Beth':{
        'phone':'1234',
        'addr':'Bar street 45'
        },
    'Cecil':{
        'phone':'4233',
        'addr':'Baz avenue 90'
        },
    'Tom':{
        'phone':'2123',
        'addr':'Cstring t 45'
        }
    }
labels={
    'phone':'phone number',
    'addr':'address'
    }
name=input("Name:")
request=input("Phone number(p) or address(a)?")
if request == 'p': key = 'phone'
if request == 'a': key = 'addr'
if name in people:
    print("%s's %s is %s." % (name,labels[key],people[name][key]))

#tempplate
template = "hi,%(name)s,your password is %(password)s"
data={'name':'silence','password':'123456'}
print(template % data)

#dictionary clear
d = {}
d['name'] = 'Gumby'
d['age'] = 43
print(d)
returnd_value = d.clear()
print(d)
print(returnd_value)
#(1)
x = {}
y = x
x['key'] = 'value'
print(y)
x = {}
print(y)
#(2)
x = {}
y = x
x['key'] = 'value'
print(y)
x.clear()
print(y)

#dictionary copy
x = {'username':'admin','machines':{'bar','foo','baz'}}
y = x.copy()
y['username'] = 'mlh'
y['machines'].remove('bar')
print(y)
print(x)
#{'machines': {'baz', 'foo'}, 'username': 'mlh'}
#{'machines': {'baz', 'foo'}, 'username': 'admin'}

#deepcopy深复制
from copy import deepcopy
d = {}
d['names'] = ['Alfred','Bertrand']
c = d.copy()
dc = deepcopy(d)
d['names'].append('Clive')
print(c)
print(dc)
#{'names': ['Alfred', 'Bertrand', 'Clive']}
#{'names': ['Alfred', 'Bertrand']}

#fromkeys使用给定的件简历新的字典，每个键都对应一个默认的值None
print("使用fromkeys使用给定的件简历新的字典")
print({}.fromkeys(['name','age']))
#此外你还可以直接在dict上面调用该方法，前面讲过，dict是所有字典的类型
print(dict.fromkeys(['names','age']))
#自己提供默认值
print(dict.fromkeys(['names','age'],'(unkonw)'))

#get function
d = {}
print(d.get('name'))
people={
    'Alice':{
        'phone':'2314',
        'addr':'Foo Driver 23'
        },
    'Beth':{
        'phone':'1234',
        'addr':'Bar street 45'
        },
    'Cecil':{
        'phone':'4233',
        'addr':'Baz avenue 90'
        },
    'Tom':{
        'phone':'2123',
        'addr':'Cstring t 45'
        }
    }
labels={
    'phone':'phone number',
    'addr':'address'
    }
name=input("Name:")
request=input("Phone number(p) or address(a)?")
if request == 'p': key = 'phone'
if request == 'a': key = 'addr'
person = people.get(name,{})
lable = labels.get(key,key)
result = person.get(key,'not available')
print("%s's %s is %s." % (name,lable,result))

