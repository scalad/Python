#string format,如果使用列表或者其他序列替代元组，那么序列会被解释成为一个值。只有元组和字典可以
format = 'hello,%s. %s enough for ya?'
values=('world','Hot')
print(format % values)

format = 'Pi with three decimals: %.3f'
from math import pi
print(format % pi)

#string template
from string import Template
s = Template('$x,glorious,$x!')
print(s.substitute(x='slurm'))
#${}
s = Template("It's ${x}tastic")
print(s.substitute(x="slurm"))
#字典变量提供值
s = Template("A $thing must never $action.")
d={}
d['thing']='sileng'
d['action']='show his socks'
print(s.substitute(d))

print("Price of eggs:$%d"%42)
print("Hebercimal price of eggs:%x"%42)
from math import pi
print("%10f" % pi)
print("%10.2f" % pi)
print("%.2f" % pi)
print("%010.2f" % pi)
#左对齐-
print("%-10.2f" % pi)





