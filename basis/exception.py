#Exception
#自己引发异常 raise Exception
#rause Exceotuib('hyoerdriver overload'),带参数的异常
#import exceptions
#dir(exceptions)系统内建的异常类

#自定义异常类
class SomeCustomeException(Exception):
    pass

#异常的捕捉
try:
    x = input('Enter the first number:')
    y = input('Enter the second number:')
    print(int(x)/int(y))
except ZeroDivisionError:
    print('The Second number can not ber zero')
#如果没有捕获异常，那么就会传播到调用的函数中，如果在该函数中没有进行捕获，那么该异常会浮动到最顶层
#如果捕获了异常还想重新引发它，那么可以调用不带参数的raise
class MuffleCalculator:
    muffled = False
    def calc(self,expr):
        try:
            return eval(expr)
        except ZeroDivisionError:
            if self.muffled:
                print('Division by zero is illegal')
            else:
                raise
#多个异常
print('多个异常')
try:
    x = input('Enter the first number:')
    y = input('Enter the second number:')
    print(int(x)/int(y))
except ZeroDivisionError:
    print('The Second number can not ber zero')
except ValueError:
    print('Thar was not a number ,was it?')

#用一个块打印多个异常
print('用一个块打印多个异常')
try:
    x = input('Enter the first number:')
    y = input('Enter the second number:')
    print(int(x)/int(y))
except (ZeroDivisionError,TypeError,ValueError,NameError):
    print('your number were bogus..')

#捕捉异常对象信息
print('捕捉异常对象信息')
try:
    x = input('Enter the first number:')
    y = input('Enter the second number:')
    print(int(x)/int(y))
except (ZeroDivisionError,TypeError,ValueError,NameError) as e:
    print(e)

try:
    print('A simple task')
except:
    print('What?Something went wrong?')
else:
    print('An...It went as planned')

while True:
    try:
        x = input('Enter the first number:')
        y = input('Enter the second number:')
        print(int(x)/int(y))
    except:
        print('Invalid input .Please try again')
    else:
        break

while True:
    try:
        x = input('Enter the first number:')
        y = input('Enter the second number:')
        print(int(x)/int(y))
    except Exception as e:
        print('Invalid input:',e)
        print('Invalid input .Please try again')
    else:
        break

#finally
x = None
try:
    x = 1/0
finally:
    print('cleaning up..')
    del x

try:
    1/0
except NameError:
    print('Unkown variable!')
else:
    print('Thar went well')
finally:
    print('cleaning up.')

