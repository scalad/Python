#构造方法
class FooBar:
    def __init__(self,value=42):
        self.somevar = value
f = FooBar()
print(f.somevar)

class A:
    def hello(self):
        print('Hello,I am A')
class B(A):
    def hello(self):
        print('Hello,I am B')
a = A()
b = B()
a.hello()
b.hello()

class Bird:
    def __init__(self):
        self.hungry = True
    def eat(self):
        if self.hungry:
            print('Aaaah...')
            self.hungry = False
        else:
            print('No,thanks you')
b = Bird()
b.eat()
b.eat()
class SongBird(Bird):
    def __init__(self):
        #Bird.__init__(self)
        self.sound = 'Squawk'
    def sing(self):
        print(self.sound)
sb = SongBird()
sb.sing()
#sb.eat()报错'SongBird' object has no attribute 'hungry'

#super函数的使用
print('Bird1 and SongBird1 function ')
class Bird1:
    def __init__(self):
        self.hungry = True
    def eat(self):
        if self.hungry:
            print('Aaaah...')
            self.hungry = False
        else:
            print('No.Thanks!')
class SongBird1(Bird1):
    def __init__(self):
        super(SongBird1,self).__init__()
        self.sound = 'Squawk'
    def sing(self):
        print(self.sound)
sb = SongBird1()
sb.sing()
sb.eat()
sb.eat()

