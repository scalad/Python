#初始化函数
def init(data):
    data['first'] = {}
    data['middle'] = {}
    data['last'] = {}
#获取名字
def lookup(data,label,name):
    return data[label].get(name)
#存储函数
def store(data,full_name):
    names = full_name.split()
    if len(names) == 2:names.insert(1,'')
    labels = 'first','middle','last'
    for label,name in zip(labels,names):
        people = lookup(data,label,name)
        if people:
            people.append(full_name)
        else:
            data[label][name] = [full_name]
myNames = {}
init(myNames)
store(myNames,'Magnuxs Lie Hetland')
result = lookup(myNames,'middle','Lie')
print(result)

store(myNames,'Robin Hood')
store(myNames,'Robin Locksley')
print(myNames)

#参数位置
def hello_1(greeting,name):
    print('%s,%s' % (greeting,name))
def hello_2(name,greeting):
    print('%s,%s' % (name,greeting))
def hello_3(greeting='hello',name='world'):
    print('%s,%s' % (greeting,name))
def hello_4(name='world',greeting='hello'):
    print('%s,%s' % (greeting,name))

#参数收集 *收集其余位置的参数
def print_params(*param):
    print(param)
def print_params_1(title,*param):
    print(title)
    print(param)
def print_params_2(**params):
    print(params)
#返回的是字典而不是元组，**收集关键字参数
print_params_2(x=1,y=2,z=3)
def print_params_3(x,y,z=3,*pospar,**keypar):
    print(x,y,z)
    print(pospar)
    print(keypar)
print_params_3(1,2,4,4,5,6,7,foo=1,bar=2)

#参数手机的逆过程
def add(x,y):
    return x + y
params=(1,2)
print(add(*params))
params={'name':'silence','greeting':'well met'}
hello_4(**params)
#在定义或者调用函数时使用星号或者双星号仅传递元组或者字典
def with_start(**kwds):
    print(kwds['name'],' is',kwds['age'],'years old')
def withoud_start(kwds):
    print(kwds['name'],' is',kwds['age'],'years old')
args = {'name':'Mr.Gumby','age':34}
with_start(**args)
withoud_start(args)
#在定义和调用函数时都使用了星号

#practice
def interval(start,stop=None,Step=1):
    'Imitates range() for step()>0'
    if stop is None:
        start,stop = 0,start
        result = []
        i = start
        while i < stop:
            result.append(i)
            i+=Step
        return result

#函数访问全局变量
def combine(parameter):
    print(parameter , globals()['parameter'])
parameter = 'berry'
combine('Shrub')

#函数嵌套
def multiplier(factor):
    def multiplyByFactor(number):
        return number*factor
    return multiplyByFactor
double = multiplier(2)
print(double(3))



