#string find() function
s = 'With a moo-moo here,and a moo-moo there'
print(s.find('moo',8))
print(s.find('moo',8,10))
print(s.find('moo'))

#string join() functinon split的逆方法
seq=['1','2','3','4','5']
sep='+'
print(sep.join(seq))
dirs = '','usr','bin','env'
print('/'.join(dirs))

#string lower() function
s = "Troudheim Hammer Dance's lower"
print(s.lower())
name="Gumbyd"
names=['gumby','smith','jones']
if name.lower() in names:
    print(name + " found it!")
else:
    print(name + " not found it")

#string title() function单词首字母大写
s = "Troudheim Hammer Dance's lower"
print(s.title())

#string replace() function
s = 'This is a test'
print(s.replace('is','eez'))

#string split() function
s = '1+2+3+4+5'
print(s.split('+'))
s='using the default'
print(s.split())

#string strip function
s='          this is strip function     '
print(s.strip())

#string transate function,和replace方法一样，但是translate只替换单个字符
table = str.maketrans('cs','kz')
print(len(table))
print("this is an incredible test".translate(table))

